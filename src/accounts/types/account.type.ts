import { Account, Address } from '@tatumio/tatum';

export interface AccountWithAddress extends Account {
  address: Address;
}
