import {
  TransferBscBep20,
  TransferCeloOrCeloErc20Token,
  TransferErc20,
  TransferTron,
} from '@tatumio/tatum';

export type TransactionBody =
  | TransferErc20
  | TransferCeloOrCeloErc20Token
  | TransferBscBep20
  | TransferTron;
