import { Currency } from '@tatumio/tatum';

export interface Wallet {
  mnemonic: string;
  xpub: string;
  currency: Currency;
}
