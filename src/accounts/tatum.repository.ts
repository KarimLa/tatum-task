import {
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import {
  Account as TatumAccount,
  Address,
  createAccount as createTatumAccount,
  createNewSubscription,
  CreateSubscription,
  Currency,
  generateDepositAddress,
  generateWallet,
  getAccountsByCustomerId as getTatumAccountsByCustomerId,
  getAllAccounts as getAllTatumAccounts,
  getDepositAddressesForAccount,
  getTransactionsByCustomer,
  sendTransaction,
  SubscriptionType,
  Transaction,
  TransactionHash,
} from '@tatumio/tatum';
import { AccountWithAddress } from './types/account.type';
import { TransactionBody } from './types/transaction.type';
import { Wallet } from './types/wallet.type';
import { Account } from './entities/account.entity';

@Injectable()
export class TatumRepostory {
  constructor(
    private readonly configService: ConfigService,
    @InjectModel(Account.name) private readonly accountModel: Model<Account>,
  ) {}

  async createAccounts(): Promise<AccountWithAddress[]> {
    const wallets = await this.genereteWallets();
    const accounts = await this.generateInternalAccount(wallets);
    const addresses = await this.generateDepositAddresses(accounts);

    return accounts.map((account, i) => ({
      ...account,
      address: addresses[i],
    }));
  }

  async getAccounts(pageSize = 50, offset = 0): Promise<TatumAccount[]> {
    try {
      const accounts = await getAllTatumAccounts(pageSize, offset);
      return accounts;
    } catch (error) {
      console.log(error);
      throw new InternalServerErrorException();
    }
  }

  async getAccount(id: string): Promise<TatumAccount[]> {
    try {
      const account = await this.accountModel
        .findOne({ customerId: id })
        .exec();

      if (!account) {
        throw new NotFoundException(`Account with address ${id} not found`);
      }

      return await getTatumAccountsByCustomerId(account.customerId);
    } catch (error) {
      console.log(error);
      throw new NotFoundException(`Account with address ${id} not found`);
    }
  }

  async getAddressesForAccount(id: string): Promise<Address[]> {
    const accounts = await this.getAccount(id);
    try {
      return await getDepositAddressesForAccount(accounts[0]?.customerId);
    } catch (error) {
      console.log(error);
      throw new InternalServerErrorException();
    }
  }

  async getTransactionsForAccount(id: string): Promise<Transaction[]> {
    const accounts = await this.getAccount(id);

    try {
      return await getTransactionsByCustomer({ id: accounts[0]?.customerId });
    } catch (error) {
      console.log(error);
      throw new InternalServerErrorException();
    }
  }

  async sendTransaction(
    currency: Currency,
    body: TransactionBody,
  ): Promise<TransactionHash> {
    try {
      return await sendTransaction(true, currency, body);
    } catch (error) {
      console.log(error);
      throw new InternalServerErrorException();
    }
  }
  async createAddressNotificationTransactionSubscription(
    address: string,
    currency: Currency,
  ) {
    try {
      const data: CreateSubscription = {
        type: SubscriptionType.ADDRESS_TRANSACTION,
        attr: {
          address,
          chain: currency,
          url: this.configService.get('WEBHOOKURL'),
        },
      };
      return await createNewSubscription(data);
    } catch (error) {
      console.log(error);
      throw new InternalServerErrorException();
    }
  }

  private async generateDepositAddresses(accounts: TatumAccount[]) {
    try {
      return await Promise.all(
        accounts.map(({ id }) => generateDepositAddress(id)),
      );
    } catch (error) {
      console.log(error);
      throw new InternalServerErrorException();
    }
  }

  private async generateInternalAccount(wallets: Wallet[]) {
    try {
      const account = new this.accountModel();
      const tatumAccounts = await this.generateTatumAccounts(account, wallets);
      // link all the created tatum accounts to our internal account
      account.customerId = tatumAccounts[0].customerId; // all the accounts have the same customerId
      await account.save();
      return tatumAccounts;
    } catch (error) {
      console.log(error);
      throw new InternalServerErrorException();
    }
  }

  private async generateTatumAccounts(account: Account, wallets: Wallet[]) {
    try {
      return await Promise.all(
        wallets.map((wallet) =>
          createTatumAccount({
            currency: wallet.currency,
            xpub: wallet.xpub,
            customer: { externalId: account.id },
          }),
        ),
      );
    } catch (error) {
      console.log(error);
      throw new InternalServerErrorException();
    }
  }

  private async genereteWallets() {
    try {
      const currecies = [Currency.ETH, Currency.CELO, Currency.MATIC];
      const wallets = await Promise.all(
        currecies.map((currency) => generateWallet(currency, true)),
      );

      return wallets.map(
        (wallet, i) => ({ ...wallet, currency: currecies[i] } as Wallet),
      );
    } catch (error) {
      console.log(error);
      throw new InternalServerErrorException();
    }
  }
}
