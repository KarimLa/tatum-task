import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import {
  Account,
  Address,
  Currency,
  Transaction,
  TransactionHash,
} from '@tatumio/tatum';
import { AccountsService } from './accounts.service';
import { AccountWithAddress } from './types/account.type';
import { TransactionBody } from './types/transaction.type';

@Controller('accounts')
export class AccountsController {
  constructor(private readonly accountsService: AccountsService) {}

  @Post('authenticate')
  authenticate(): Promise<AccountWithAddress[]> {
    return this.accountsService.authenticate();
  }

  @Get()
  listAccounts(): Promise<Account[]> {
    return this.accountsService.listAccounts();
  }

  @Get(':id')
  getAccount(@Param('id') id: string): Promise<Account[]> {
    return this.accountsService.getAccount(id);
  }

  @Get(':id/address')
  getAddressesForAccount(@Param('id') id: string): Promise<Address[]> {
    return this.accountsService.getAddressesForAccount(id);
  }

  @Get(':id/transaction')
  getTransactionsForAccount(@Param('id') id: string): Promise<Transaction[]> {
    return this.accountsService.getTransactionsForAccount(id);
  }

  @Post(':currency/transaction')
  createTransactionsForAccount(
    @Param('currency') currency: string,
    @Body() body: TransactionBody,
  ): Promise<TransactionHash> {
    // TODO: validate transacation body
    // Note: I do not completely understand the shape of transaction body
    // So I did not validate it.
    return this.accountsService.sendTransacation(Currency[currency], body);
  }

  @Post('notification')
  tatumWebhook(@Body() body) {
    // TODO(): set a websocket connection and feed the data coming
    // from tatum's api to the frontend
    console.log(body);
    return { ok: true };
  }

  @Get('notification/:address/:currency/enable')
  enableNotifcation(
    @Param('address') address: string,
    @Param('currency') currency: string,
  ) {
    return this.accountsService.enableNotification(address, Currency[currency]);
  }
}
