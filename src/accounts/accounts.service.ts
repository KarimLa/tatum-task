import { Injectable } from '@nestjs/common';
import {
  Account,
  Address,
  Currency,
  Transaction,
  TransactionHash,
} from '@tatumio/tatum';
import { TatumRepostory } from './tatum.repository';
import { AccountWithAddress } from './types/account.type';
import { TransactionBody } from './types/transaction.type';

@Injectable()
export class AccountsService {
  constructor(private readonly tatumRepo: TatumRepostory) {}

  async authenticate(): Promise<AccountWithAddress[]> {
    return this.tatumRepo.createAccounts();
  }

  async listAccounts(): Promise<Account[]> {
    return this.tatumRepo.getAccounts();
  }

  async getAccount(id: string): Promise<Account[]> {
    return this.tatumRepo.getAccount(id);
  }

  async getAddressesForAccount(id: string): Promise<Address[]> {
    return this.tatumRepo.getAddressesForAccount(id);
  }

  async getTransactionsForAccount(id: string): Promise<Transaction[]> {
    return this.tatumRepo.getTransactionsForAccount(id);
  }

  async sendTransacation(
    currency: Currency,
    body: TransactionBody,
  ): Promise<TransactionHash> {
    return this.tatumRepo.sendTransaction(currency, body);
  }

  async enableNotification(address: string, currency: Currency) {
    return this.tatumRepo.createAddressNotificationTransactionSubscription(
      address,
      currency,
    );
  }
}
