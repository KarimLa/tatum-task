# Tatum Task

A task for Tatum's (https://tatum.io) backend developer position

Production env (https://tatumtask.herokuapp.com/accounts/)
Staging env (https://tatumtask-staging.herokuapp.com/accounts/)

## Getting started

Copy .env.example to .env

bash `mv .env.example .env`

Add your Tatum API key TATUM_API_KEY

Start docker containers

bash `docker-compose up -d`

Run application

bash `yarn start:dev`

## Project documentation

I have created an API that implements the different steps in the task.

### API

Route: http://localhost:3000/accounts/authenticate
Method: POST
Access: Public
Description:

- on success:
  - creates three accounts with wallet and address for ETH CILO and MATIC
  - Links the accounts to a customer id and saves it in the database
- on error:
  - 500 if we could not fetch data from the Tatum API

Route: http://localhost:3000/accounts/
Method: GET
Access: Public
Description:

- on success: returns a list of Tatum virtual accounts
- on error:
  - 500 if we could not fetch data from the Tatum API

Route: http://localhost:3000/accounts/:id
Param: - id: customer id
Method: GET
Access: Public
Description:

- on success: fetches the accounts using the customer id and returns a list of Tatum virtual accounts
- on error:
  - 404 if we could not find the customer
  - 500 if we could not fetch data from the Tatum API

Route: http://localhost:3000/accounts/:id/address
Param: - id: account id
Method: GET
Access: Public
Description:

- on success: returns address for the account
- on error:
  - 404 if we could not find the account
  - 500 if we could not fetch data from the Tatum API

Route: http://localhost:3000/accounts/:id/transactions
Param: - id: customer id
Method: GET
Access: Public
Description:

- on success: returns a list of transactions for a cusotmer
- on error:
  - 404 if we could not find the customer account
  - 500 if we could not fetch data from the Tatum API

Route: http://localhost:3000/accounts/:currency/transactions
Param: - currency: cryptocurrency
Body: - Body: Transaction body (not validated)
Method: POST
Access: Public
Description:

- on success: returns the transaction hash
- on error:
  - 500 if we could not fetch data from the Tatum API

Route: http://localhost:3000/accounts/notification
Method: POST
Access: Public
Description:

- on success: returns a success response

Route: http://localhost:3000/accounts/notification/:address/:currency/enable
Param: - address: wallet address - currency: cryptocurrency type
Method: GET
Access: Public
Description:

- on success: creates a new notification subscription for the given address and currency and then returns the id that comes from `createNewSubscription`
- on error:
  - 500 if we could not fetch data from the Tatum API
